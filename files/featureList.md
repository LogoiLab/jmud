Features
==
- __CannedResponses__: Produces randomized fuzzy responses for every command unless verbosity is enabled. These are designed to be witty. Also a dispatcher for Randomize.

- __Hunger__: Items that are food have a value that can restore health. Items that are poison or dangerous can damage health. Health is a percentage that cannot exceed 100%. Eating more food than fills health is a waste of food. The `health` command displays a fuzzy response as to the state of your health unless verbosity is set.

- __LightLevelCommand__: Light levels play a major role in describing a room around you. Lighting may also decide if it is dangerous to be in a place and punish you for being there. Items held by the player can produce light to supplement the light level of the current room. The darker the room the more dangerous it is. The `light` command will return a fuzzy description of the light level unless verbosity is set.

- __LockedExits__: Exits can be locked by both the player and the game, a player cannot enter a locked room. Moving through a locked exit is not allowed.

- __Randomize__: Takes into account the light level, the current time, and your current health to determine if something bad will happen to you. A lot of math goes into this function. First, the values for light, health, and time are placed on a percentage scale. For light chances are higher in the dark. For health chances are higher if you're hurt. For time chances are higher if it's night. It then takes the inverse of this percentage (if chance is 80% inverse is 20%) it generates a random number within the range of 0 and the inverse. If this random number is larger than half of the inverse CannedResponses is called to dispatch a player's fate.

- __SafeSave__: The game automatically saves on death creating a deathpoint.sav file. The game also saves on fatal errors creating a EMERGENCY_BACKUP.sav file.

- __Time__: Time is divided into to segments, the actual meatspace time and the cyberspace tick. The meatspace time is used by Randomize and also handles day night cycles. The cyberspace tick is incremented every time the player issues a command. Standard to the DnD style a cyberspace tick is considered to be 6 meatspace seconds. A user can issue the `time` command to receive a fuzzy description of the current time unless verbosity is set.

- __VerboseCommand__: Modifies how responses are made to give the user more input as to the state of the game. Using `verbose` command toggles verbosity.
