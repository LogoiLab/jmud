use std::env;
use std::fs::File;
use std::io::prelude::*;

fn parse_sections(mut lines: Vec<String>) -> Vec<String> {
    println!("{:?}", &lines);
    let mut dungeon: Vec<String> = vec!();
    dungeon.push(String::from("[dungeon]"));
    let d_name: String = String::from("name = \"") + lines.remove(0).as_str() + "\"";
    dungeon.push(d_name);
    let d_version: String = String::from("version = \"") + lines.remove(0).as_str() + "\"";
    dungeon.push(d_version);
    dungeon.push(String::from("\n"));
    lines.remove(0);
    dungeon.push(String::from("[items]"));
    lines.remove(0);
    let mut item_values: Vec<String> = vec!();
    let item_index = lines.iter().position(|ref r| r == &&String::from("===")).unwrap();
    for _ in 0 .. item_index {
        item_values.push(lines.remove(0));
    }
    println!("{:?}", item_values);
    for n in 0 .. item_values.len() {
        if &item_values[n] == &String::from("===") {
            item_values.remove(n);
        }
    }
    let mut traits: Vec<String> = vec!();
    let mut actions: Vec<String> = vec!();
    for line in item_values {
        if line != String::from("---") {
            match line.parse::<usize>() {
                Ok(_) => traits.push(line),
                Err(_) => {
                    actions.push(line);
                },
            };
        } else {
            let name: String = actions.remove(0);
            dungeon.push(String::from("[items.") + &name + "]");
            match traits.len() {
                0 => panic!("There must be at least one numeric value for item: \'{}\'.", name),
                1 => {
                    dungeon.push(String::from("weight = \"") + traits.remove(0).trim() + "\"");
                },
                2 => {
                    dungeon.push(String::from("weight = \"") + traits.remove(0).trim() + "\"");
                    dungeon.push(String::from("brightness = \"") + traits.remove(0).trim() + "\"");
                },
                3 => {
                    dungeon.push(String::from("weight = \"") + traits.remove(0).trim() + "\"");
                    dungeon.push(String::from("brightness = \"") + traits.remove(0).trim() + "\"");
                    dungeon.push(String::from("nutrition = \"") + traits.remove(0).trim() + "\"");
                },
                _ => panic!("Too many numeric traits for item: \'{}\'.", name),
            };
            if actions.len() > 0 {
                for _ in 0 .. actions.len() {
                    let mut temp: Vec<String> = actions.remove(0).split(":").map(|s| s.to_string()).collect();
                    if temp.len() > 1 {
                        dungeon.push(String::from(temp.remove(0).trim()) + " = \"" + temp.remove(0).trim() + "\"");
                    } else {
                        dungeon.push(String::from(temp.remove(0).trim()) + " = \"\"");
                    }
                }
            }
        }
    }
    dungeon.push(String::from("\n[rooms]"));
    lines.remove(0);
    lines.remove(0);
    let mut room_values: Vec<String> = vec!();
    let rooms_index = lines.iter().position(|ref r| r == &&String::from("===")).unwrap();
    for _ in 0 .. rooms_index {
        room_values.push(lines.remove(0));
    }
    println!("{:?}", room_values);
    for n in 0 .. room_values.len() {
        if &room_values[n] == &String::from("===") {
            room_values.remove(n);
        }
    }
    let mut room_parts: Vec<String> = vec!();
    for line in room_values {
        if &line == &String::from("===") {
            continue;
        }
        if line != String::from("---") {
            room_parts.push(line);
        } else {
            let name: String = room_parts.remove(0);
            dungeon.push(String::from("[rooms.") + &name + "]");
            let mut contents: Vec<String> = room_parts.remove(0).split(":").map(|s| s.to_string()).collect();
            if contents.len() > 1 {
                let items: Vec<String> = contents.remove(1).split(",").map(|s| s.to_string()).collect();
                let mut toml_items = String::from("{");
                for item in items {
                    toml_items = toml_items + "\"" + item.trim() + "\",";
                }
                let len: usize = toml_items.len();
                toml_items.remove(len - 1);
                toml_items = toml_items + "}";
                dungeon.push(String::from("contents: = ") + toml_items.as_str());
                dungeon.push(lines.remove(0));
            } else {
                dungeon.push(String::from("description = \"") + contents.remove(0).as_str() + "\"");
            }
        }
    }
    dungeon.push(String::from("\n"));
    dungeon
}

fn read_file(path: &String) -> File {
    let f = match File::open(path) {
        Ok(file) => file,
        Err(e) => panic!("File not found at path: \'{}\'. {}", path, e)
    };
    return f;
}

fn main() {
    let args: Vec<_> = env::args().collect();
    let mut oldf: File = read_file(&args[1]);
    let mut contents = String::new();
    match oldf.read_to_string(&mut contents) {
        Ok(o) => o,
        Err(e) => panic!("Could not read contents of '{}' {}", &args[1], e)
    };
    println!("{}", contents);
    let cont_vec: Vec<String> = contents.split("\n").map(|s| s.to_string()).collect();
    let mut buffer = match File::create("dungeon.bork.toml") {
        Ok(o) => o,
        Err(e) => panic!("File could not be created. {}", e),
    };
    for line in parse_sections(cont_vec) {
        buffer.write(&line.as_str().as_bytes());
        if line != String::from("\n") {
            buffer.write(b"\n");
        }
    }
}
