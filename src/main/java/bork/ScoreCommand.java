/**
 * @author cbaxter
 * @version 3.2.0
 */

package bork;

/**
 * An implementation of Command used to handle the removal of an Item from the Dungeon.
 */
public class ScoreCommand extends Command {
  ScoreCommand() {

  }

  /**
   * Removes the Item from the Dungeon.
   * @return A message to be displayed by Interpreter.
   */
  String execute(){
    GameState gs = GameState.sharedInstance();
    CannedResponses cr = new CannedResponses();
    return cr.getResponse("SCORE", ""+gs.getScore());
  }
}
