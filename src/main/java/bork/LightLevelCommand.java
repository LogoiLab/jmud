/**
 * @author cbaxter
 * @version 3.2.0
 */

package bork;

class LightLevelCommand extends Command{

  /**
   * Constructor for objects of class LightLevelCommand
   */
  LightLevelCommand(){

  }

  /**
   * Executes a LightLevelCommand.
   * @return A message to Interpreter to be displayed on stdout.
   * @see Interpreter
   */
  String execute(){
    CannedResponses cr = new CannedResponses();
    GameState gs = GameState.sharedInstance();
    if(gs.getAdventurersCurrentRoom().getLightLevel() > 100){
      return cr.getResponse("LIGHTLEVEL", ""+ 100);
    } else {
      return cr.getResponse("LIGHTLEVEL", ""+ gs.getAdventurersCurrentRoom().getLightLevel());
    }
  }
}
