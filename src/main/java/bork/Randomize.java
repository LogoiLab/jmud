/**
 * @author cbaxter
 * @version 3.2.0
 */

package bork;

class Randomize {

  private static Randomize instance;
  private double chance;
  private double healthChance = chance;
  private double lightChance = chance;
  private double timeChance = chance;

  /**
   * Initializes the Random class.
   * @param chance The percentage chance.
   */
  Randomize(double chance) {
    setChance(chance);
  }

  /**
   * Single instance insurer.
   * @return The singular instance.
   */
  static synchronized Randomize sharedInstance(){
    if (instance==null){
      instance=new Randomize(0);
    }
    return instance;
  }

  /**
   * Gets the current global chance of ocurrence value.
   * @return The current chance percentage.
   */
  double getChance() {
    return this.chance;
  }

  /**
   * Gets the current health chance of ocurrence value.
   * @return The current chance percentage.
   */
  double getHealthChance() {
    return this.healthChance;
  }

  /**
   * Gets the current light chance of ocurrence value.
   * @return The current chance percentage.
   */
  double getLightChance() {
    return this.lightChance;
  }

  /**
   * Gets the current time chance of ocurrence value.
   * @return The current chance percentage.
   */
  double getTimeChance() {
    return this.timeChance;
  }

  /**
   * Sets the global chance occurrence value.
   * @param chance A percent value relative to the chances of something occuring.
   */
  void setChance(double chance) {
    this.chance = chance;
  }

  /**
   * Sets the health chance occurrence value.
   * @param chance A percent value relative to the chances of something occuring.
   */
  void setHealthChance(double chance) {
    this.healthChance = chance;
  }

  /**
   * Sets the light chance occurrence value.
   * @param chance A percent value relative to the chances of something occuring.
   */
  void setLightChance(double chance) {
    this.lightChance = chance;
  }

  /**
   * Sets the time chance occurrence value.
   * @param chance A percent value relative to the chances of something occuring.
   */
  void setTimeChance(double chance) {
    this.timeChance = chance;
  }

  private boolean chanceMe(double num) {
    double full = (Math.abs(num - 100)) * (new java.util.Random()).nextDouble();
    if (full > ((Math.abs(num - 100)) / 2)) {
      return true;
    } else {
      return false;
    }
  }

  void gen() {
    CannedResponses cr = new CannedResponses();
    if ( this.healthChance > this.lightChance && this.healthChance > this.timeChance ){
      if (chanceMe(this.healthChance)) {
        System.out.println(cr.getResponse("RANDOM","HEALTH"));
      }
    } else if ( this.lightChance > this.healthChance && this.lightChance > this.timeChance ){
      if (chanceMe(this.lightChance)) {
        System.out.println(cr.getResponse("RANDOM","LIGHT"));
      }
    } else if ( this.timeChance > this.healthChance && this.timeChance > this.lightChance ){
      if (chanceMe(this.timeChance)) {
        System.out.println(cr.getResponse("RANDOM","TIME"));
      }
    }
  }

}
