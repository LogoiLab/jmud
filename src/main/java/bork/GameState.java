/**
 * @author cmistr
 * @version 3.2.0
 */

package bork;

import org.json.*;
import java.util.Scanner;
import java.util.Hashtable;
import java.io.*;
import java.util.Set;
import java.util.ArrayList;

class GameState
{
  private static GameState instance;
  private Dungeon currentDungeon;
  private Room currentRoom;
  private Hashtable<String, Item> adventurersInventory;
  private int health;
  private int score;
  private int hunger = 50;
    private boolean verboseMode;

  /**
   * Constructor for objects of class GameState
   */
  GameState() {
    adventurersInventory=new Hashtable<String, Item>();
    this.health = 100;
    this.score = 0;
  }

  /**
   * Class initializer
   * @param d The Dungeon to be used.
   */
  void initialize(Dungeon d)
  {
    currentDungeon= d;
    currentRoom= d.getEntry();
  }

  /**
   * Single instance insurer.
   * @return The singlular instance.
   */
  static synchronized GameState sharedInstance()
  {
    if (instance==null)
    {
      instance=new GameState();
    }
    return instance;
  }

  /**
   * Gets the Room an adventurer is currently in.
   * @return The current Room.
   */
  Room getAdventurersCurrentRoom()
  {
    return currentRoom;
  }

  /**
   * Sets the Room an adventurer is currently in.
   * @param r The Room to set.
   */
  void setAdventurersCurrentRoom(Room r)
  {
    int level = 0;
    ArrayList<Item> temp = new ArrayList<Item>(this.adventurersInventory.values());
    for(Item item : temp) {
      level += item.getBrightness();
    }
    r.setLightLevel(level);
    currentRoom=r;
  }

  /**
   * Gets the current Dungeon.
   * @return The current Dungeon.
   */
  Dungeon getDungeon()
  {
    return currentDungeon;
  }

  /**
   * Adds an Item to an adventurer's inventory.
   * @param i The Item to add.
   */
  void addItemToAdventurersInventory(Item i)
  {
    adventurersInventory.put(i.toString(), i);
  }

  /**
   * Removes an item from an adventurer's inventory.
   * @param itemName      The Hash name of the Item to remove.
   * @return              The removed Item.
   */
  Item removeItemFromAdventurersInventory(String itemName)
  {
    Item i=adventurersInventory.get(itemName);
    adventurersInventory.remove(itemName);
    return i;
  }

  Hashtable<String, Item> getAdventurersInventory() {
    return this.adventurersInventory;
  }

  /**
   * Gets an Item that is in the adventurer's inventory.
   * @param  itemName      The Hash name of the Item to locate.
   * @return               The located Item.
   */
  Item getItem(String itemName)
  {
    return adventurersInventory.get(itemName);
  }

  /**
   * Lists the current inventory of an adventurer.
   * @return A serialized version of the adventurer's inventory.
   */
  String listInventory()
  {
    String toReturn="";
    Set<String> keys = adventurersInventory.keySet();
    if (keys.size()>0)
    {
      toReturn="\n";
      for (String key : keys)
      {
        toReturn+="a "+key+"\n";
      }
    }
    else
    {
      toReturn="Nothing.";
    }
    return toReturn;
  }

  /**
   * Test if an Item is in the adventurer's inventory.
   * @param itemName      The Hash name of an Item to locate.
   * @return              A true or false value.
   */
  boolean adventurerHasItem(String itemName)
  {
    Set<String> keys = adventurersInventory.keySet();
    for (String key : keys)
    {
      if (itemName.equals(key))
      {
        return true;
      }
    }
    return false;
  }

  /**
   * Adds damage to adventurer, dropping the health value.
   * @param int damage the amount of damage for the adventurer to take.
   */
  void wound (int damage) {
    if (this.health - damage > 0) {
      this.health -= damage;
    } else {
      die();
    }
  }

  /**
   * Adventurer seppuku.
   * @return You have died.
   */
  void die() {
    this.health = 0;
    System.out.println("You have died.");
    store("deathpoint");
    System.exit(0);
  }

  /**
   * Adventurer wins.
   * @return You have won!
   */
  String win() {
    return "You have won!";
  }

  /**
   * Adds to an adventurer's score.
   * @param amount The amount to add.
   */
  void addScore(int amount) {
    this.score += amount;
  }

  int getScore() {
    return this.score;
  }

  int getHealth() {
    return this.health;
  }

  void setHealth(int hea) {
    if (hea >= 0 && hea <= 100) {
      this.health = hea;
    }
  }

  int getHunger() {
    return this.hunger;
  }

  void setHunger(int hun) {
    if (hun >= 0 && hun <= 4) {
      this.hunger = hun;
    }
  }

  void transform(String old, String nnew) {
    removeItemFromAdventurersInventory(old);
    addItemToAdventurersInventory(currentDungeon.getItemByName(nnew));
  }

    void setVerboseMode(boolean b)
    {
        verboseMode=b;
    }
    boolean verbose()
    {
        return verboseMode;
    }

    boolean getVerbose() {
      return verboseMode;
    }

  /**
   * Generate a save file.
   * @param saveName The filename of the save file to be saved.
   */
  void store(String saveName)
  {
      JSONObject j=new JSONObject();
      JSONObject adventurer=new JSONObject();
      adventurer.put("currentRoom", currentRoom.getTitle());
      JSONArray advInv=new JSONArray();
      ArrayList<Item> itemList=new ArrayList<Item>(adventurersInventory.values());
      for (int i=0; i<itemList.size(); i++)
      {
          advInv.put(itemList.get(i).toString());
      }


      adventurer.put("inventory", advInv);
      adventurer.put("score", score);
      adventurer.put("health", health);

      j.put("adventurer", adventurer);
      j.put("dungeonFile", currentDungeon.getFileName());
      j.put("dungeon", currentDungeon.serialize());

    try
    {
      FileWriter fw=new FileWriter(saveName+".sav");
      BufferedWriter out=new BufferedWriter(new PrintWriter(fw));
        out.write(j.toString());
      out.flush();
      out.close();
    }
    catch (IOException e)
    {
      System.err.println(e);
    }
  }

  /**
   * loads a save file.
   * @param fileName                       The path to a save file.
   * @throws FileNotFoundException         Thrown when the save file cannot be found.
   * @throws IllegalDungeonFormatException Thrown when the Dungeon format is unreadable.
   * @throws IllegalSaveFormatException    Thrown when the save format is unreadable.
   */
  void restore(String fileName) throws FileNotFoundException, IllegalDungeonFormatException, IllegalSaveFormatException
  {
    Scanner in=null;
    try
    {
      in=new Scanner(new File(fileName));
    }
    catch (IOException e)
    {
      throw new FileNotFoundException("Save file not found.");
    }
      try
      {
          JSONObject obj=new JSONObject(in.nextLine());
          JSONObject dungeon = obj.getJSONObject("dungeon");
          JSONArray rooms = dungeon.getJSONArray("rooms");
          JSONObject adventurer = obj.getJSONObject("adventurer");
          currentDungeon = null;
          try
          {
              currentDungeon = new Dungeon(obj.getString("dungeonFile"));
          }
          catch (IllegalDungeonFormatException e)
          {
              throw e;
          }
          currentDungeon.clearItemsInAllRooms();
          Hashtable<String, Item> items=currentDungeon.getItems();
          for (int i=0; i<rooms.length(); i++)
          {
              JSONObject room=rooms.getJSONObject(i);
              Room r=currentDungeon.getRoom(room.getString("roomName"));
              r.setBeenHere(room.getBoolean("beenHere"));
              JSONArray roomItems=room.getJSONArray("contents");
              for (int j=0; j<roomItems.length(); j++)
              {
                  r.addItem(items.get(roomItems.getString(j)));
              }
              r.setLightLevel(room.getInt("lightLevel"));
          }
          JSONArray advInv = adventurer.getJSONArray("inventory");
          for (int i=0; i<advInv.length(); i++)
          {
              addItemToAdventurersInventory(items.get(advInv.getString(i)));
          }
          Time.sharedInstance().setTick(dungeon.getInt("tick"));
          score=adventurer.getInt("score");
          health=adventurer.getInt("health");
          setAdventurersCurrentRoom(currentDungeon.getRoom(adventurer.getString("currentRoom")));
      }
      catch (Exception e)
      {
          throw new IllegalSaveFormatException("Invalid Save File");
      }
  }
}
