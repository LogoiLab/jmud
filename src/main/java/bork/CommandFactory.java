/**
 * @author cmistr
 * @version 3.2.0
 */

package bork;

/**
 * A singleton class designed to produce the correct Command object based on
 * String input
 */
class CommandFactory
{
  private static CommandFactory instance;
  private char dirs [] ={'u', 'd', 'n', 's', 'e', 'w'};

  CommandFactory()
  {

  }

  /**
   * Single instance insurer.
   * @return The singular instance.
   */
  static synchronized CommandFactory sharedInstance()
  {
    if (instance==null)
    {
      instance=new CommandFactory();
    }
    return instance;
  }

  /**
   * A stdin command parser.
   * @param command       The String version of a Command passed by Interpreter.
   * @return        A deserialized Command or an UnknownCommand.
   */
  Command parse(String command){
    Time.sharedInstance().doTick();
    if (command.contains("drop"))
    {
      if (command.length()>4)
      {
        return new DropCommand(command.substring(5));
      }
      return new DropCommand("");
    }
    else if (command.contains("take"))
    {
      if (command.length()>4)
      {
        return new TakeCommand(command.substring(5));
      }
      return new TakeCommand("");
    }
    else if (command.contains("health"))
    {
      return new HealthCommand();
    }
    else if (command.contains("disappear"))
    {
      GameState gs = GameState.sharedInstance();
      return new DisappearCommand(gs.getDungeon().getItemByName(command.split(" ")[1]));
    }
    else if (command.contains("hunger"))
    {
      return new HungerCommand();
    }
    else if (command.contains("light"))
    {
      return new LightLevelCommand();
    }
    else if (command.contains("time"))
    {
      return new TimeCommand();
    }
    else if (command.contains("teleport"))
    {
      GameState gs = GameState.sharedInstance();
      return new TeleportCommand(gs.getAdventurersCurrentRoom(), gs.getDungeon().getRoom(command.split("teleport")[1].trim()));
    }
    else if (command.contains("transform"))
    {
      GameState gs = GameState.sharedInstance();
      return new TransformCommand(gs.getDungeon().getItemByName(command.split(" ")[1]), gs.getDungeon().getItemByName(command.split(" ")[2]));
    }
    else if (command.contains("score"))
    {
      return new ScoreCommand();
    }
    else if (command.contains("verbose"))
    {
      return new VerboseCommand();
    }
    else if (command.equals("i"))
    {
      return new InventoryCommand();
    }
    else if (command.contains("save"))
    {
      if (command.length() > 4)
      {
        return new SaveCommand(command.substring(command.indexOf(" ")+1));
      }
      return new SaveCommand("");
    }
    else if (command.contains(" "))
    {
      return new ItemSpecificCommand(command.substring(command.indexOf(" ")+1), command.substring(0, command.indexOf(" ")));
    }
    else
    {
      for (char c : dirs)
      {
        if (command.charAt(0)==c)
        {
          String s ="";
          s+=c;
          return new MovementCommand(s);
        }
      }
    }
    return new UnknownCommand();
  }
}
