/**
 * @author zeitz
 * @version 3.2.0
 */

package bork;

abstract class Command {

  /**
   * A promise for a Command output.
   * @return Output to be printed to stdout.
   */
  abstract String execute();
}
