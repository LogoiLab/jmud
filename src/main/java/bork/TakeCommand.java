/**
 * @author cmistr
 * @version 3.2.0
 */

package bork;

class TakeCommand extends Command {
  private String itemName;

  /**
   * Creates a TakeCommand class.
   * @param toTake The hash name of an item to take.
   */
  TakeCommand(String toTake) {
    itemName=toTake;
  }

  /**
   *Executes the command and returns a displayable String for Interpreter.
   * @return A human readable String.
   * @see Interpreter
   */
  String execute() {
    String toReturn="";
    GameState gs=GameState.sharedInstance();
    if (itemName.equals("")) {
      toReturn="Take what?";
    }
    else {
      if (gs.getAdventurersCurrentRoom().itemExists(itemName)) {
        gs.addItemToAdventurersInventory(gs.getAdventurersCurrentRoom().takeItem(itemName));
        CannedResponses cr = new CannedResponses();
        toReturn = cr.getResponse("TAKE", itemName);
      }
      else {
        toReturn = "There is no "+itemName+" in here.";
      }
    }
    return toReturn;
  }
}
