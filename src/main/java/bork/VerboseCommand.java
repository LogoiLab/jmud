/**
 * @author
 * @version 3.2.0
 */

package bork;

class VerboseCommand extends Command {

  /**
   *Creates a VerboseCommand class.
   */
  VerboseCommand(){
  }

  /**
   *Executes the command and returns a displayable String for Interpreter.
   * @return A human readable String.
   * @see Interpreter
   */
  String execute(){
    GameState gs = GameState.sharedInstance();
    gs.setVerboseMode(!gs.getVerbose());
    CannedResponses cr = new CannedResponses();
    return cr.getResponse("VERBOSE", ""+gs.getVerbose());
  }
}
