/**
 * @author cmistr
 * @version 3.2.0
 */

package bork;

import org.json.*;
import java.util.Hashtable;
import java.util.Scanner;
import java.util.Collection;
import java.util.ArrayList;
import java.io.*;

/**
 * A working model of the enitre map and its Contents
 */
public class Dungeon
{
  private String name;
  private Room entry;
  private Hashtable<String, Room> rooms;
  private Hashtable<String, Item> items;
  private String fileName;

  public Dungeon(String fileName) throws FileNotFoundException, IllegalDungeonFormatException
  {
    try
    {
        if (isOldFile(fileName))
        {
            System.out.println("\nWARNING: This is an old file format. Not all features will be supported.\n");
            readFromOldFile(fileName);
        }
        else
        {
            readFromFile(fileName);
        }
    }
    catch (FileNotFoundException e)
    {
      throw e;
    }
    catch (IllegalDungeonFormatException e)
    {
      throw e;
    }
  }
  public Dungeon(String inName, Room inEntry)
  {
    name=inName;
    entry=inEntry;
    rooms=new Hashtable<String, Room>();
  }

    boolean isOldFile(String fileName) throws FileNotFoundException
    {
        Scanner in=null;
        try
        {
            in=new Scanner(new File(fileName));
        }
        catch (IOException e)
        {
            throw new FileNotFoundException("File not found.");
        }
        String read="";
        while (in.hasNext())
        {
            read+=in.next();
        }
        if (read.contains("==="))
        {
            return true;
        }
        return false;

    }
    /**
     * Reads a Dungeon object from a bork-json file.
     * @param  fileName                      The path to a bork file to use.
     * @throws FileNotFoundException         Thrown when a file is not found.
     * @throws IllegalDungeonFormatException Thrown when Dungeon file is not of the correct format.
     */
    void readFromFile(String fileName) throws FileNotFoundException, IllegalDungeonFormatException
    {
        this.fileName=fileName;
        rooms=new Hashtable<String, Room>();
        items=new Hashtable<String, Item>();
        Scanner in=null;
        try
        {
            in=new Scanner(new File(fileName));
        }
        catch (IOException e)
        {
            throw new FileNotFoundException("File not found.");
        }
        String jsonString="";
        in.useDelimiter("\n");
        while (in.hasNext())
        {
            jsonString+=in.next();
        }
        JSONObject dungeon=new JSONObject(jsonString);
        JSONArray itemsArr = dungeon.getJSONArray("items");
        for (int i=0; i<itemsArr.length(); i++)
        {
            JSONObject anItem = itemsArr.getJSONObject(i);
            Item item = new Item(anItem.getString("itemName"), anItem.getInt("itemWeight"), anItem.getBoolean("producesLight"), anItem.getInt("brightness"));
            JSONArray itemActions = anItem.getJSONArray("actions");
            for (int j=0; j<itemActions.length(); j++)
            {
                JSONObject action = itemActions.getJSONObject(j);
                String transformTo=action.getString("transformTo");
                String actionName=action.getString("actionName");
                String actionResult=action.getString("actionResult");
                boolean shouldDisappear=action.getBoolean("shouldDisappear");
                boolean shouldTeleport=action.getBoolean("shouldTeleport");
                boolean shouldKillAdventurer = action.getBoolean("shouldKillAdventurer");
                boolean shouldCauseWin=action.getBoolean("shouldCauseWin");
                int score=action.getInt("score");
                int woundValue=action.getInt("woundValue");
                item.addAction(new Action(actionName, item.toString(), actionResult, shouldCauseWin, shouldKillAdventurer, shouldTeleport, shouldDisappear, transformTo, score, woundValue));
            }
            items.put(item.toString(), item);
        }


        JSONArray roomsArray = dungeon.getJSONArray("rooms");
        for (int i=0; i<roomsArray.length(); i++)
        {
            JSONObject room = roomsArray.getJSONObject(i);
            Room r = new Room(room.getString("roomName"), room.getInt("lightLevel"));
            JSONArray roomContents = room.getJSONArray("contents");
            for (int j=0; j<roomContents.length(); j++)
            {
                r.addItem(items.get(roomContents.getString(j)));
            }
            r.setDescription(room.getString("description"));
            rooms.put(r.getTitle(), r);
            if (entry==null)
            {
                entry=r;
            }
        }

        JSONArray exitsArray = dungeon.getJSONArray("exits");
        for (int i=0; i<exitsArray.length(); i++)
        {
            JSONObject exit = exitsArray.getJSONObject(i);
            Room r = rooms.get(exit.getString("existsInRoom"));
            String dir = exit.getString("direction");
            String dest = exit.getString("destination");
            r.addExit(new Exit(dir, r, rooms.get(dest)));
        }
    }
  /**
   * Reads a Dungeon object from a bork file.
   * @param  fileName                      The path to a bork file to use.
   * @throws FileNotFoundException         Thrown when a file is not found.
   * @throws IllegalDungeonFormatException Thrown when Dungeon file is not of the correct format.
   */
  void readFromOldFile(String fileName) throws IllegalDungeonFormatException, FileNotFoundException
  {
    this.fileName=fileName;
    rooms=new Hashtable<String, Room>();
    items=new Hashtable<String, Item>();
    Scanner in=null;
    try
    {
      in=new Scanner(new File(fileName));
    }
    catch (IOException e)
    {
        throw new FileNotFoundException("File not found.");
    }
    in.useDelimiter("\n");
    String read="";
    String name=in.next();
    String ver=in.next();
    if (!(ver.equals("Group Bork v1.0") || ver.equals("Bork v3.0")))
    {
      throw new IllegalDungeonFormatException("Incompatible Dungeon File.");
    }
    String type="";
    read=in.next();
    while (in.hasNext())
    {
      if (read.contains("==="))
      {
        type=in.next();
        read=in.next();
      }
      if (!read.equals("==="))
      {
        if (type.equals("Items:"))
        {
          String itemName=read;
          int itemWeight=Integer.valueOf(in.next());
          Item i=new Item(itemName, itemWeight);
          String s=in.next();
          while (!s.equals("---"))
          {
              String transformTo="";
              String actionName="";
              String actionResult="";
              boolean shouldDisappear=false;
              boolean shouldTeleport=false;
              boolean shouldKillAdventurer = false;
              boolean shouldCauseWin=false;
              int score=0;
              int woundValue=0;
              if (s.contains("["))
              {
                  actionName=s.substring(0, s.indexOf("["));
                  String brackets=s.substring(s.indexOf("[")+1, s.indexOf("]"));
                  if (brackets.indexOf("Transform") != -1)
                  {
                      String temp=brackets.substring(brackets.indexOf("Transform("));
                      if (temp.contains(","))
                      {
                          transformTo=temp.substring(temp.indexOf("(") + 1, temp.indexOf(",")-1);
                      }
                      else
                      {
                          transformTo=temp.substring(temp.indexOf("(") + 1, temp.indexOf(")"));
                      }
                  }
                  if (brackets.indexOf("Wound") != -1)
                  {
                      String temp=brackets.substring(brackets.indexOf("Wound"));
                      if (temp.contains(","))
                      {
                          woundValue=Integer.valueOf(temp.substring(temp.indexOf("(") + 1, temp.indexOf(",")-1));
                      }
                      else
                      {
                          woundValue=Integer.valueOf(temp.substring(temp.indexOf("(") + 1, temp.indexOf(")")));

                      }
                  }
                  if (brackets.indexOf("Score") != -1)
                  {
                      String temp=brackets.substring(brackets.indexOf("Score"));
                      if (temp.contains(","))
                      {
                          score=Integer.valueOf(temp.substring(temp.indexOf("(") + 1, temp.indexOf(",")-1));
                      }
                      else
                      {
                          score=Integer.valueOf(temp.substring(temp.indexOf("(") + 1, temp.indexOf(")")));
                      }
                  }
                  if (brackets.indexOf("Disappear") != -1)
                  {
                      shouldDisappear=true;
                  }
                  if (brackets.indexOf("Teleport") != -1)
                  {
                      shouldTeleport=true;
                  }
                  if (brackets.indexOf("Die") != -1)
                  {
                      shouldKillAdventurer=true;
                  }
                  if (brackets.indexOf("Win") != -1)
                  {
                      shouldCauseWin=true;
                  }
              }
              else
              {
                  actionName=s.substring(0, s.indexOf(":"));
              }
              actionResult=s.substring(s.indexOf(":")+1);
              i.addAction(new Action(actionName, i.toString(), actionResult, shouldCauseWin, shouldKillAdventurer, shouldTeleport, shouldDisappear, transformTo, score, woundValue));
            s=in.next();
          }
          items.put(i.toString(), i);
          read=in.next();
        }
        else if (type.equals("Rooms:"))
        {
          Room r=new Room(read);
          read=in.next();
          if (read.contains("Contents:"))
          {
            String temp=read;
            temp=temp.substring(temp.indexOf(":")+2);
            while (temp.contains(","))
            {
              r.addItem(items.get(temp.substring(0, temp.indexOf(","))));
              temp=temp.substring(temp.indexOf(",")+1);
            }
            r.addItem(items.get(temp));
            read=in.next();
          }
            in.useDelimiter("---");
            read+=in.next();
          r.setDescription(read);
          rooms.put(r.getTitle(), r);
          in.useDelimiter("\n");
          read=in.next();
          read=in.next();
          if (entry==null)
          {
            entry=r;
          }
        }
        else if (type.equals("Exits:"))
        {
          Room r=rooms.get(read);
          String dir=in.next();
          String dest=in.next();
          r.addExit(new Exit(dir, r, rooms.get(dest)));
          in.next();
          read=in.next();
        }
        else
        {
          throw new IllegalDungeonFormatException("Invalid Dungeon File.");
        }
      }
      else
      {
        read=in.next();
      }
    }
    in.close();
  }

  /**
   * Returns the Dungeon name;
   * @return The name feild from Dungeon
   */
  String getName()
  {
    return name;
  }

  /**
   * Adds a Room to the rooms Hashtable.
   * @param r The Room object to add.
   */
  void add(Room r)
  {
    rooms.put(r.getTitle(), r);
  }

  /**
   * Locates a specific Room object within the rooms Hashtable.
   * @param roomTitle     The hash of the Room object that has been located, also the Room's name.
   * @return              The located Room object.
   */
  Room getRoom(String roomTitle)
  {
    return rooms.get(roomTitle);
  }

  /**
   * Locates the Dungeon's entrypoint.
   * @return A Room that is the Dungeon's entrypoint.
   */
  Room getEntry()
  {
    return entry;
  }

  /**
   * Gets the path to the currently in use bork file.
   * @return A path.
   */
  String getFileName()
  {
    return fileName;
  }

  /**
   * Gets all the items stored in the Dungeon.
   * @return The items field.
   */
  Hashtable<String, Item> getItems()
  {
    return items;
  }

  Item getItemByName(String itemName) {
    return items.get(itemName);
  }

  /**
   * Removes all the items in a Room.
   */
  void clearItemsInAllRooms()
  {
    ArrayList<Room> roomObjs=new ArrayList<Room>(rooms.values());
    for (Room r : roomObjs)
    {
      r.removeAllItems();
    }
  }

  /**
   * Removes an item fron the Dungeon.
   * @param i The item to be removed.
   */
  void disappearItem(Item i) {
    items.remove(i.toString());
  }

  /**
   * Object serializer for a bork file.
   * @return A serialized bork file String.
   */
  JSONObject serialize()
  {
      JSONObject o = new JSONObject();
    ArrayList<Room> roomObjs=new ArrayList<Room>(rooms.values());
      JSONArray roomArray = new JSONArray();
    for (Room r : roomObjs)
    {
        JSONObject thisRoom=new JSONObject();
        thisRoom.put("roomName", r.getTitle());
        thisRoom.put("beenHere", r.hasBeenHere());

      ArrayList<Item> itemList=new ArrayList<Item>(r.getItems().values());
        JSONArray contents=new JSONArray();
      if (itemList.size()>0)
      {
        for (int i=0; i<itemList.size(); i++)
        {
            contents.put(itemList.get(i).toString());
        }
      }
        thisRoom.put("contents", contents);
        thisRoom.put("lightLevel", r.getLightLevel());
        roomArray.put(thisRoom);
    }
      o.put("tick", Time.sharedInstance().getTick());
      o.put("rooms", roomArray);
      return o;
  }
}
