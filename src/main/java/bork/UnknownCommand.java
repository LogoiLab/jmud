/**
 * @author cmistr
 * @version 3.2.0
 */

package bork;

class UnknownCommand extends Command {
  /**
   * Creates an UnknownCommand class.
   */
  UnknownCommand() {

  }

  /**
   *Executes the command and returns a displayable String for Interpreter.
   * @return A human readable String.
   */
  String execute() {
    CannedResponses cr = new CannedResponses();
      return cr.getResponse("UNKNOWN", "");
  }
}
