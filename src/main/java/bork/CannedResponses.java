/**
* @author cbaxter
* @version 3.2.0
*/

package bork;

import java.lang.Math;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Random;

/**
 * A kind and witty response generator.
 */
class CannedResponses {

  /**
   * The Types enum that contains all Command types.
   */
  private enum Types {
    ;
    static final String DISAPPEAR = "DISAPPEAR";
    static final String DROP = "DROP";
    static final String HEALTH = "HEALTH";
    static final String HUNGER = "HUNGER";
    static final String INVENTORY = "INVENTORY";
    static final String ITEMSPECIFIC = "ITEMSPECIFIC";
    static final String LIGHTLEVEL = "LIGHTLEVEL";
    static final String MOVEMENT = "MOVEMENT";
    static final String RANDOM = "RANDOM";
    static final String SAVE = "SAVE";
    static final String SCORE = "SCORE";
    static final String TAKE = "TAKE";
    static final String TELEPORT = "TELEPORT";
    static final String TIME = "TIME";
    static final String TRANSFORM = "TRANSFORM";
    static final String UNKNOWN = "UNKNOWN";
    static final String VERBOSE = "VERBOSE";
  }

  private void err() {
    System.out.println("Something really bad has happened to your game engine!/nFORCING SAVE!");
    GameState.sharedInstance().store("EMERGENCY_BACKUP.sav");
    System.out.println("Exiting to maintain system integrity.");
    System.exit(0);
  }

  CannedResponses() {

  }

  /**
   * Returns a response based on input.
   * @param commandType   The type of command asking for a response.
   * @param include       Info to include in the response.
   * @return              A response.
   */
   String getResponse(String commandType, String include) {
    String resp = "";
    if (!GameState.sharedInstance().verbose()) {
      switch (commandType) {
        case Types.DISAPPEAR : resp = disappear(include);
                  break;
        case Types.DROP : resp = drop(include);
                  break;
        case Types.HEALTH : resp = health(include);
                  break;
        case Types.HUNGER : resp = hunger(include);
                  break;
        case Types.INVENTORY : resp = inventory(include);
                  break;
        case Types.ITEMSPECIFIC : resp = itemspecific(include);
                  break;
        case Types.LIGHTLEVEL : resp = lightlevel(include);
                  break;
        case Types.MOVEMENT : resp = movement(include);
                  break;
        case Types.RANDOM : resp = random(include);
                  break;
        case Types.SAVE : resp = save(include);
                  break;
        case Types.SCORE : resp = score(include);
                  break;
        case Types.TAKE : resp = take(include);
                  break;
        case Types.TELEPORT : resp = teleport(include);
                  break;
        case Types.TIME : resp = time(include);
                  break;
        case Types.TRANSFORM : resp = transform(include);
                  break;
        case Types.UNKNOWN : resp = unknown();
                  break;
        case Types.VERBOSE : resp = verbose(include);
                  break;
        default : err();
                  break;
      }
    } else {
      switch (commandType) {
        case Types.DISAPPEAR : resp = "Disappear command was run against: " + include;
                  break;
        case Types.DROP : resp = "Drop command was run against: " + include;
                  break;
        case Types.HEALTH : resp = "Your current health is: " + include;
                  break;
        case Types.HUNGER : resp = "Your current hunger level is: " + include;
                  break;
        case Types.INVENTORY : resp = inventory(include);
                  break;
        case Types.ITEMSPECIFIC : resp = itemspecific(include);
                  break;
        case Types.LIGHTLEVEL : resp = "The current light level is: " + include;
                  break;
        case Types.MOVEMENT : resp = movement(include);
                  break;
        case Types.RANDOM : resp = "Random: " + include+ " " + random(include);
                  break;
        case Types.SAVE : resp = save(include);
                  break;
        case Types.SCORE : resp = score(include);
                  break;
        case Types.TAKE : resp = "Take command was run against: " + include;
                  break;
        case Types.TELEPORT : resp = "Teleport command was run against: " + include;
                  break;
        case Types.TIME : resp = "The current time is: " + include;
                  break;
        case Types.TRANSFORM : resp = "Transform command was run against: " + include;
                  break;
        case Types.UNKNOWN : resp = unknown();
                  break;
        case Types.VERBOSE : resp = verbose(include);
                  break;
        default : err();
                  break;
      }
    }
    return resp;
  }

  /**
   * Creates a canned response for DisappearCommand.
   * @param  include Info to be included in response.
   * @return         The witty response.
   */
  private String disappear(String include) {
    String resp = "";
    Random r = new Random();
    switch (r.nextInt(4)) {//edit rnd nextint to match case amt.
      case 0 : resp = "Bloop, " + include + " is gone.";
               break;
      case 1 : resp = include + " was stolen by a grue.";
               break;
      case 2 : resp = include + " was eaten by a rust monster.";
               break;
      case 3 : resp = include + " dematerializes in your hand.";
               break;
      case 4 : resp = include + " disappears before your eyes.";
               break;
      default : err();
                break;
    }
    return resp;
  }

  /**
   * Creates a canned response for DropCommand.
   * @param  include Info to be included in response.
   * @return         The witty response.
   */
  private String drop(String include) {
    String resp = "";
    Random r = new Random();
    switch (r.nextInt(2)) {//edit rnd nextint to match case amt.
      case 0 : resp = "Tink, " + include + " skitters across the floor.";
               break;
      case 1 : resp = include + " slips from your grasp.";
               break;
      case 2 : resp = include + " drops to the ground.";
               break;
      default : err();
                break;

    }
    return resp;
  }

  /**
   * Creates a canned response for HealthCommand.
   * @param  include Info to be included in response.
   * @return         The witty response.
   */
  private String health(String include) {
    String resp = "";
    switch ((int)Math.ceil(Integer.parseInt(include) / 10)) {
      case 1 : resp = "You are bleeding out.";
               break;
      case 2 : resp = "You are bleeding profusely.";
               break;
      case 3 : resp = "Tis but a scratch!";
               break;
      case 4 : resp = "You are maimed.";
               break;
      case 5 : resp = "Multiple lacerations.";
               break;
      case 6 : resp = "Multiple contusions.";
               break;
      case 7 : resp = "A few scratches.";
               break;
      case 8 : resp = "A few bruises.";
               break;
      case 9 : resp = "You are under the weather.";
               break;
      case 10 : resp = "It's a new day, the sun is shining, the tank is clean...";
               break;
      default : err();
                break;
    }
    return resp;
  }

  /**
   * Creates a canned response for HungerCommand.
   * @param  include Info to be included in response.
   * @return         The witty response.
   */
  private String hunger(String include) {
    String resp = "";
    switch ((int)Math.floor(Integer.parseInt(include) % 4)) {
      case 0 : resp = "You're dying of starvation!";
                 break;
      case 1 : resp = "You could eat a horse.";
                 break;
      case 2 : resp = "A home cooked meal could really hit the spot.";
                 break;
      case 3 : resp = "You feel a bit peckish.";
                 break;
      case 4 : resp = "Happier than the knights of the kitchen table.";
                  break;
      default : err();
                break;
    }
    return resp;
  }

  /**
   * Creates a canned response for InventoryCommand.
   * @param  include Info to be included in response.
   * @return         The witty response.
   */
  private String inventory(String include) {
    return "You're currently carrying: " + include;
  }

  /**
   * Creates a canned response for ItemSpecificCommand.
   * @param  include Info to be included in response.
   * @return         The witty response.
   */
  private String itemspecific(String include) {
    return include;
  }

  /**
   * Creates a canned response for LightLevelCommand.
   * @param  include Info to be included in response.
   * @return         The witty response.
   */
  private String lightlevel(String include) {
    String resp = "";
    switch ((int)Math.ceil(Integer.parseInt(include) / 10)) {
      case 1 : resp = "You can't see your hand in front of your face.";
               break;
      case 2 : resp = "only a few feet in any direction can be seen. You are likely to be eaten by a grue.";
               break;
      case 3 : resp = "As if by candlelight you are cautious.";
               break;
      case 4 : resp = "Moody";
               break;
      case 5 : resp = "Like a sunset on the beach.";
               break;
      case 6 : resp = "Filtered light emanates around you.";
               break;
      case 7 : resp = "Colors are sharp, everything is quite visible.";
               break;
      case 8 : resp = "Like Mid-day sun.";
               break;
      case 9 : resp = "You squint, saturated light removes the color from the world around you.";
               break;
      case 10 : resp = "You are surrounded by blinding brightness. You cannot see, for you stare into a sun.";
               break;
      default : err();
                break;
    }
    return resp;
  }

  /**
   * Creates a canned response for MovementCommand.
   * @param  include Info to be included in response.
   * @return         The witty response.
   */
  private String movement(String include) {
    String resp = "";
    Random r = new Random();
    switch (r.nextInt(4)) {//edit rnd nextint to match case amt.
      case 0 : resp = "You head to " + include;
               break;
      case 1 : resp = "You saunter to " + include;
               break;
      case 2 : resp = "You mosey to " + include;
               break;
      case 3 : resp = "You walk to " + include;
               break;
      case 4 : resp = "You move to " + include;
               break;
      default : err();
                break;
    }
    return resp;
  }

  /**
   * Creates a canned response for Random.
   * @param  include Info to be included in response.
   * @return         The witty response.
   */
  private String random(String include) {
    String resp = "";
    Random r = new Random();
    GameState gs = GameState.sharedInstance();
    if(include == "HEALTH"){
      switch (r.nextInt(1)){
        case 0 : {resp = "You are bleeding out! If you do not heal soon you will die!"; gs.wound(1);}
          break;
        case 1 : {resp = "Your wounds continue to do damage! If you do not heal soon you will die!"; gs.wound(1);}
          break;
        default : err();
          break;
      }
    }
    if(include == "LIGHT"){
      switch (r.nextInt(1)){
        case 0 : {
            ArrayList<Item> items = new ArrayList<Item>(gs.getAdventurersInventory().values());
            resp = "It's so dark, you think you feel your inventory get lighter. But you can't be sure.";
            gs.removeItemFromAdventurersInventory(items.get(r.nextInt(items.size() - 1)).toString());
          }
          break;
        case 1 : {resp = "You can't see the floor. You trip and fall!"; gs.wound(1);}
          break;
        default : err();
          break;
      }
    }
    if(include == "TIME"){
      switch (r.nextInt(1)){
        case 0 : {resp = "It's night time, the Veshta Nerada are out! They nibble your toes; it burns."; gs.wound(1);}
          break;
        case 1 : {
            ArrayList<Item> items = new ArrayList<Item>(gs.getAdventurersInventory().values());
            resp = "A theif of the night backstabs you and steals one of your items!";
            gs.removeItemFromAdventurersInventory(items.get(r.nextInt(items.size() - 1)).toString());
            gs.wound(5);
          }
          break;
        default : err();
          break;

      }
    }

    return resp;
  }

  /**
   * Creates a canned response for SaveCommand.
   * @param  include Info to be included in response.
   * @return         The witty response.
   */
  private String save(String include) {
    return include;
  }

  /**
   * Creates a canned response for ScoreCommand.
   * @param  include Info to be included in response.
   * @return         The witty response.
   */
  private String score(String include) {
    return "Your current score is: " + include;
  }

  /**
   * Creates a canned response for TakeCommand.
   * @param  include Info to be included in response.
   * @return         The witty response.
   */
  private String take(String include) {
    String resp = "";
    Random r = new Random();
    switch (r.nextInt(4)) {//edit rnd nextint to match case amt.
      case 0 : resp = "You hastily grab the " + include;
               break;
      case 1 : resp = "You carefully pick up the " + include;
               break;
      case 2 : resp = "You tout the " + include + " in your hands, as if to guess its weight.";
               break;
      case 3 : resp =  include + " is now in your hands.";
               break;
      case 4 : resp = "You are the proud owner of " + include + ". Take good care of it!";
               break;
      default : err();
                break;
    }
    return resp;
  }

  /**
   * Creates a canned response for TeleportCommand.
   * @param  include Info to be included in response.
   * @return         The witty response.
   */
  private String teleport(String include) {
    String resp = "";
    Random r = new Random();
    switch (r.nextInt(3)) {//edit rnd nextint to match case amt.
      case 0 : resp = "Your head spins as you blink to " + include;
               break;
      case 1 : resp = "You have been beemed to " + include;
               break;
      case 2 : resp = "Your feel a slight tingling sensation. " + include + " materializes around you.";
               break;
      case 3 : resp =  include + " blinks into existance";
               break;
      default : err();
                break;
    }
    return resp;
  }

  /**
   * Creates a canned response for TimeCommand.
   * @param  include Info to be included in response.
   * @return         The witty response.
   */
  private String time(String include) {
    String resp = "";
    switch ((int)Math.ceil(Integer.parseInt(include) / 10)) {
      case 1 : resp = "You feel tired, you have been wandering through the night.";
               break;
      case 2 : resp = "Your best programming occurs now.";
               break;
      case 3 : resp = "Dim light breaks the horizon.";
               break;
      case 4 : resp = "It's morning, you smell eggs and bacon.";
               break;
      case 5 : resp = "The sun blazes directly overhead.";
               break;
      case 6 : resp = "A wee bit past mid-day.";
               break;
      case 7 : resp = "The day starts to grow to an end.";
               break;
      case 8 : resp = "Dusk darkens the word around you.";
               break;
      case 9 : resp = "Night has fallen.";
               break;
      case 10 : resp = "The moon is high above you.";
               break;
      default : err();
               break;
    }
    return resp;
  }

  /**
   * Creates a canned response for TransformCommand.
   * @param  include Info to be included in response.
   * @return         The witty response.
   */
  private String transform(String include) {
    String resp = "";
    Random r = new Random();
    switch (r.nextInt(1)) {//edit rnd nextint to match case amt.
      case 0 : resp = include.split("`")[0] + " shimmers turning into " + include.split("`")[1];
               break;
      case 1 : resp = include.split("`")[0] + " winks out of existance and " + include.split("`")[1] + " materializes in your hand.";
               break;
      default : err();
               break;
    }
    return resp;
  }

  /**
   * Creates a canned response for UnknownCommand.
   * @return         The witty response.
   */
  private String unknown() {
    String resp = "";
    Random r = new Random();
    switch (r.nextInt(2)) {//edit rnd nextint to match case amt.
      case 0 : resp = "I didn't catch that.";
               break;
      case 1 : resp = "Command not understood.";
               break;
      case 2 : resp = "Please try again.";
               break;
      default : err();
               break;
    }
    return resp;
  }

  /**
   * Creates a canned response for VerboseCommand.
   * @param  include Info to be included in response.
   * @return         The witty response.
   */
  private String verbose(String include) {
    return "Verbosity: " + include;
  }

}
