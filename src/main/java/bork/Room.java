/**
 * @author cmistr
 * @version 3.2.0
 */

package bork;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Set;

public class Room
{
  private String title;
  private String description;
  private boolean beenHere;
  private int lightLevel;
  private ArrayList<Exit> exits;
  private Hashtable<String, Item> items;

  /**
   * Constructor for objects of class Room
   * @param inTitle       Desired title of this Room object
   * @param lightLevel    the light level of the room.
   */
  public Room(String inTitle, int lightLevel) {
    this.title = inTitle;
    this.description = "";
    this.beenHere = false;
    this.lightLevel = lightLevel;
    this.exits = new ArrayList<Exit>();
    this.items = new Hashtable<String, Item>();
  }

  /**
   * Constructor for objects of class Room
   * @param inTitle       Desired title of this Room object
   */
  public Room(String inTitle) {
    this.title = inTitle;
    this.description = "";
    this.beenHere = false;
    this.lightLevel = 45;
    this.exits = new ArrayList<Exit>();
    this.items = new Hashtable<String, Item>();
  }

  /**
   * Returns the title of this Room
   * @return Title
   */
  String getTitle()
  {
    return title;
  }
  /**
   * Sets the description of this Room
   * @param desc Description to set
   */
  void setDescription(String desc)
  {
    description=desc;
  }
  /**
   * Adds an Item to this Room's collection
   * @param item Item object to add to collection
   */
  void addItem(Item item)
  {
    if(item.doesProduceLight()){
      this.lightLevel += item.getBrightness();
    }
    items.put(item.toString(), item);
  }
  /**
   * Returns an Item, and removes it from this Room
   * @param itemName      Name of item to take
   * @return              The Item taken.
   */
  Item takeItem(String itemName)
  {
    Item i=items.get(itemName);
    this.lightLevel -= i.getBrightness();
    items.remove(i.toString());
    return i;
  }
  /**
   * Removes all items in this Room
   */
  void removeAllItems()
  {
    items=new Hashtable<String, Item>();
  }
  /**
   * Checks whether or not a specific Item exists in this Room
   * @param  itemName      Name of Item to check for
   * @return               True or false, depending on whether or not item exists. True
   * if it does, false if it doesn't.
   */
  boolean itemExists(String itemName)
  {
    Set<String> keys = items.keySet();
    for(String key: keys)
    {
      if (key.equals(itemName))
      {
        return true;
      }
    }
    return false;
  }
  /**
   * Gets the Items in this Room
   * @return A Hashtable contining all the items in this Room
   */
  Hashtable<String, Item> getItems()
  {
    return items;
  }

  /**
   * returns the lightLevel of the item.
   * @return lightLevel
   */
  int getLightLevel() {
    return this.lightLevel;
  }

  /**
   * sets the lightLevel of an object.
   * @param int lightLevel the lightLevel.
   */
  void setLightLevel(int lightLevel) {
    this.lightLevel = lightLevel;
  }
  /**
   * Describes this Room, and sets BeenHere to true.
   * @return A String containing the description of this Room
   */
  String describe()
  {
    String tempDesc=title;
    if (!beenHere)
    {
      beenHere=true;
      tempDesc+=":\n\n"+description+"\n";
    }
    else
    {
      tempDesc+="\n";
    }
    for (Exit e : exits)
    {
      tempDesc+="\n"+e.describe();
    }
    Set<String> keys = items.keySet();
    tempDesc+="\n";
    for(String key: keys)
    {
      tempDesc+="\nThere is a "+key+" here.";
    }
    return tempDesc;
  }
  /**
   * Gets the next room based on an exit, if it exists
   * @param dir           The direction of exit to use
   * @return              The destination Room if it exists, null if it does not
   */
  Room leaveBy(String dir)
  {
    for (Exit e : exits)
    {
      if (e.getDir().equals(dir))
      {
          if (!e.isLocked())
          {
              return e.getDest();
          }
      }
    }
    return null;
  }
  /**
   * Adds an exit to this Room's collection
   * @param e The Exit object to add
   */
  void addExit(Exit e)
  {
    exits.add(e);
  }
  /**
   * Sets the BeenHere value for this Room
   * @param hasBeenHere Desired value for BeenHere
   */
  void setBeenHere(boolean hasBeenHere)
  {
    beenHere=hasBeenHere;
  }
  /**
   * Returns whether or not the adventurer has been in this Room
   * @return True if adventurer has been here, false if not
   */
  boolean hasBeenHere()
  {
    return beenHere;
  }
}
