/**
 * @author cbaxter
 * @version 3.2.0
 */

package bork;

class Action {

  private String name;
  private String itemName;
  private String result;
  private boolean shouldCauseWin;
  private boolean shouldCauseDeath;
  private boolean shouldTeleportItem;
  private boolean shouldDisappear;
  private String shouldTransformTo;
  private int score = 0;
  private int wound = 0;

  Action(String name, String itemName, String result, boolean shouldCauseWin, boolean shouldCauseDeath, boolean shouldTeleportItem, boolean shouldDisappear, String shouldTransformTo, int score, int wound) {
    this.name = name;
    this.itemName = itemName;
    this.result = result;
    this.shouldCauseDeath = shouldCauseDeath;
    this.shouldTeleportItem = shouldTeleportItem;
    this.shouldDisappear = shouldDisappear;
    this.shouldTransformTo = shouldTransformTo;
    this.score = score;
    this.wound = wound;
  }

  public String toString() {
    return this.name;
  }

  String getResult() {
    GameState gs = GameState.sharedInstance();
    if(shouldCauseWin) {
      gs.win();
    }
    if(shouldCauseDeath) {
      System.out.println(this.result);
      gs.die();
    }
    if(shouldDisappear) {
      gs.removeItemFromAdventurersInventory(this.itemName);
    }
    if(!shouldTransformTo.equals("")) {
      gs.transform(this.itemName, this.shouldTransformTo);
    }
    gs.addScore(this.score);
    gs.wound(this.wound);
    return this.result;
  }



}
