/**
 * @author cbaxter
 * @version 3.2.0
 */

package bork;

/**
 * An implementation of Command used to handle the removal of an Item from the Dungeon.
 */
public class DisappearCommand extends Command {
  private Item item;

  /**
   * @param i An item object to be removed.
   */
  DisappearCommand(Item item) {
    this.item = item;
  }

  /**
   * Removes the Item from the Dungeon.
   * @return A message to be displayed by Interpreter.
   */
  String execute(){
    CannedResponses cr = new CannedResponses();
    GameState gs = GameState.sharedInstance();
    gs.getDungeon().disappearItem(this.item);
    return cr.getResponse("DISAPPEAR", this.item.toString());
  }
}
