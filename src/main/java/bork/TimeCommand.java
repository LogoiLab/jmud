/**
 * @author cbaxter
 * @version 3.2.0
 */

package bork;

class TimeCommand extends Command{

  /**
   *Creates a TimeCommand class.
   */
  TimeCommand(){

  }

  /**
   *Executes the command and returns a displayable String for Interpreter.
   * @return A human readable String.
   */
  String execute(){
    CannedResponses cr = new CannedResponses();
    Time t = Time.sharedInstance();
    return "You have made: " + t.getTick() + " movements. " + cr.getResponse("TIME", ""+t.getTime());//TEMP!
  }
}
