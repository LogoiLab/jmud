/**
 * @author cmistr
 * @version 3.2.0
 */

package bork;

class InventoryCommand extends Command {
  /**
   * Constructor for objects of class InventoryCommand
   */
  InventoryCommand() {

  }
  /**
   * Executes an InventoryCommand
   * @return A message to Interpreter to be displayed on stdout
   */
  String execute() {
    CannedResponses cr = new CannedResponses();
    return cr.getResponse("INVENTORY", GameState.sharedInstance().sharedInstance().listInventory());
  }
}
