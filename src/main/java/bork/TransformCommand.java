/**
 * @author cbaxter
 * @version 3.2.0
 */

package bork;

class TransformCommand extends Command{
  private Item oldItem;
  private Item newItem;

  /**
   * Creats a TransformCommand class.
   */
  TransformCommand(Item oldItem, Item newItem){
    this.oldItem = oldItem;
    this.newItem = newItem;
  }

  /**
   *Executes the command and returns a displayable String for Interpreter.
   * @return A human readable String.
   * @see Interpreter
   */
  String execute(){
    GameState gs = GameState.sharedInstance();
    gs.removeItemFromAdventurersInventory(this.oldItem.toString());
    gs.addItemToAdventurersInventory(this.newItem);
    CannedResponses cr = new CannedResponses();
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    //!OUTPUT IS "<olditem>`<newitem>" A STRING DELIMITED WITH "`" (BACKTICK) THAT CONTAINS THE NAME OF FIRST THEN OLD THEN THE NEW ITEM!
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    return cr.getResponse("TRANSFORM", this.oldItem.toString() + "`" + this.newItem.toString());
  }
}
