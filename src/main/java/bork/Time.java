/**
 * @author cbaxter
 * @version 3.2.0
 */

package bork;
import java.util.Hashtable;
import java.math.BigInteger;
import java.time.LocalTime;

class Time {
  private static Time instance;
  private Integer currentTick;
  private Integer currentTime;
  private Hashtable<Integer, Integer> timeTable = new Hashtable<Integer, Integer>();

  /**
   * Initializes time class.
   * @param tick The current tick value.
   * @param time The current time in miliseconds.
   */
  Time() {
    this.currentTick = Integer.valueOf(0);
    this.currentTime = Integer.valueOf(0);
  }

  /**
   * Single instance insurer.
   * @return The singular instance.
   */
  static synchronized Time sharedInstance(){
    if (instance==null){
      instance=new Time();
    }
    return instance;
  }

  /**
   * Gets the curent in-game tick.
   * @return The current tick integer.
   */
  int getTick() {
    return this.currentTick;
  }

  /**
   * Sets the current in-game tick.
   * @param tick The tick value to set.
   */
  void setTick(int tick) {
    this.currentTick = Integer.valueOf(tick);
    this.addToTimeTable();
  }

  /**
   * Increments the game tick.
   */
  void doTick() {
    this.currentTick++;
    this.currentTime = LocalTime.now().getHour();
    this.addToTimeTable();
    doARandom();
  }

  /**
   * Gets the current in-game time.
   * @return The current time in miliseconds
   */
  int getTime() {
    return this.currentTime.intValue();
  }

  /**
   * Sets the current in-game time.
   * @param time The time value in miliseconds to set.
   */
  void setTime(int time) {
    this.currentTime = Integer.valueOf(time);
    this.currentTick++;
    this.addToTimeTable();
  }

  /**
   * Adds both tick and time values to the timetable for storage.
   */
  private void addToTimeTable() {
    this.timeTable.put(this.currentTick, this.currentTime);
  }

  /**
   * Preprocesses the time table for saving to a save file.
   * @return A serialized String to save.
   */
  String timeTableToString() {
    return "";
  }

  void doARandom() {
    Randomize rand = Randomize.sharedInstance();
    GameState gs = GameState.sharedInstance();
    rand.setHealthChance(Math.abs(100.0 - gs.getHealth()));
    rand.setLightChance(Math.abs(100.0 - (gs.getAdventurersCurrentRoom().getLightLevel() / 100)));
    switch (getTime()) {
      case 0 : rand.setTimeChance(80.0);
        break;
      case 1 : rand.setTimeChance(90.0);
        break;
      case 2 : rand.setTimeChance(85.0);
        break;
      case 3 : rand.setTimeChance(80.0);
        break;
      case 4 : rand.setTimeChance(60.0);
        break;
      case 5 : rand.setTimeChance(45.0);
        break;
      case 6 : rand.setTimeChance(35.0);
        break;
      case 7 : rand.setTimeChance(20.0);
        break;
      case 8 : rand.setTimeChance(10.0);
        break;
      case 9 : rand.setTimeChance(5.0);
        break;
      case 10 : rand.setTimeChance(5.0);
        break;
      case 11 : rand.setTimeChance(5.0);
        break;
      case 12 : rand.setTimeChance(5.0);
        break;
      case 13 : rand.setTimeChance(5.0);
        break;
      case 14 : rand.setTimeChance(5.0);
        break;
      case 15 : rand.setTimeChance(5.0);
        break;
      case 16 : rand.setTimeChance(5.0);
        break;
      case 17 : rand.setTimeChance(10.0);
        break;
      case 18 : rand.setTimeChance(20.0);
        break;
      case 19 : rand.setTimeChance(35.0);
        break;
      case 20 : rand.setTimeChance(45.0);
        break;
      case 21 : rand.setTimeChance(60.0);
        break;
      case 22 : rand.setTimeChance(80.0);
        break;
      case 23 : rand.setTimeChance(85.0);
        break;
      default : rand.setTimeChance(90.0);
        break;
    }
  }

}
