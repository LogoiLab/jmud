/**
 * @author cmistr
 * @version 3.2.0
 */

package bork;

public class IllegalSaveFormatException extends Exception {
  /**
   * Default constrcutor for IllegalSaveFormatException objects
   */
  public IllegalSaveFormatException() {

  }
  /**
   * Constructor for IllegalSaveFormatException objects
   * @param inMessage     Exception message
   */
  public IllegalSaveFormatException(String inMessage) {
    super(inMessage);
  }
}
