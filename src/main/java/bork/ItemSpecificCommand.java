/**
 * @author cmistr
 * @version 3.2.0
 */

package bork;

class ItemSpecificCommand extends Command {
  private String item;
  private String action;

  /**
   * Constructor for objects of class ItemSpecificCommand
   * @param inItem of Item
   * @param inAction to execute on this Item
   */
  ItemSpecificCommand(String inItem, String inAction) {
    item=inItem;
    action=inAction;
  }
  /**
   * Executes an ItemSpecificCommand.
   * @return A message to Interpreter to be displayed on stdout.
   */
  String execute() {
    String toReturn="";
    GameState gs = GameState.sharedInstance();
    if (gs.adventurerHasItem(item)) {
      CannedResponses cr = new CannedResponses();
      toReturn = cr.getResponse("ITEMSPECIFIC", gs.getItem(item).getMessageForAction(action));
    } else {
      toReturn = "You don't have a "+item+".";
    }
    return toReturn;
  }
}
