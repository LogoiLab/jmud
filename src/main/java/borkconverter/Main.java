
package borkconverter;

import java.util.*;
import org.json.*;
import java.io.*;

public class Main
{
    public static void main(String args[])
    {
        if (args.length < 1)
        {
            System.err.println("Usage: Main <bork file>");
        }
        else
        {
            convertFile(args[0]);
        }
    }
    public static void convertFile(String fileName)
    {
        JSONObject obj=new JSONObject();
        JSONArray rooms=new JSONArray();
        JSONArray items=new JSONArray();
        JSONArray exits = new JSONArray();
        Scanner in=null;
        try
        {
            in=new Scanner(new File(fileName));
        }
        catch (IOException e)
        {
            System.err.println(e);
        }
        in.useDelimiter("\n");
        String read="";
        String name=in.next();
        String ver=in.next();
        if (!(ver.equals("Group Bork v1.0") || ver.equals("Bork v3.0")))
        {
            System.err.println("Incompatible File");
        }
        String type="";
        read=in.next();
        while (in.hasNext())
        {
            if (read.contains("==="))
            {
                type=in.next();
                read=in.next();
            }
            if (!read.equals("==="))
            {
                if (type.equals("Items:"))
                {
                    String itemName=read;
                    int itemWeight=Integer.valueOf(in.next());
                    JSONObject i=new JSONObject();
                    JSONArray itemActions=new JSONArray();
                    i.put("itemName", itemName);
                    i.put("itemWeight", itemWeight);
                    
                    String s=in.next();
                    while (!s.equals("---"))
                    {
                        String transformTo="";
                        String actionName="";
                        String actionResult="";
                        boolean shouldDisappear=false;
                        boolean shouldTeleport=false;
                        boolean shouldKillAdventurer = false;
                        boolean shouldCauseWin=false;
                        int score=0;
                        int woundValue=0;
                        if (s.contains("["))
                        {
                            actionName=s.substring(0, s.indexOf("["));
                            String brackets=s.substring(s.indexOf("[")+1, s.indexOf("]"));
                            if (brackets.indexOf("Transform") != -1)
                            {
                                String temp=brackets.substring(brackets.indexOf("Transform("));
                                if (temp.contains(","))
                                {
                                    transformTo=temp.substring(temp.indexOf("(") + 1, temp.indexOf(",")-1);
                                }
                                else
                                {
                                    transformTo=temp.substring(temp.indexOf("(") + 1, temp.indexOf(")"));
                                }
                            }
                            if (brackets.indexOf("Wound") != -1)
                            {
                                String temp=brackets.substring(brackets.indexOf("Wound"));
                                if (temp.contains(","))
                                {
                                    woundValue=Integer.valueOf(temp.substring(temp.indexOf("(") + 1, temp.indexOf(",")-1));
                                }
                                else
                                {
                                    woundValue=Integer.valueOf(temp.substring(temp.indexOf("(") + 1, temp.indexOf(")")));
                                    
                                }
                            }
                            if (brackets.indexOf("Score") != -1)
                            {
                                String temp=brackets.substring(brackets.indexOf("Score"));
                                if (temp.contains(","))
                                {
                                    score=Integer.valueOf(temp.substring(temp.indexOf("(") + 1, temp.indexOf(",")-1));
                                }
                                else
                                {
                                    score=Integer.valueOf(temp.substring(temp.indexOf("(") + 1, temp.indexOf(")")));
                                }
                            }
                            if (brackets.indexOf("Disappear") != -1)
                            {
                                shouldDisappear=true;
                            }
                            if (brackets.indexOf("Teleport") != -1)
                            {
                                shouldTeleport=true;
                            }
                            if (brackets.indexOf("Die") != -1)
                            {
                                shouldKillAdventurer=true;
                            }
                            if (brackets.indexOf("Win") != -1)
                            {
                                shouldCauseWin=true;
                            }
                        }
                        else
                        {
                            actionName=s.substring(0, s.indexOf(":"));
                        }
                        actionResult=s.substring(s.indexOf(":")+1);
                        
                        JSONObject action = new JSONObject();
                        action.put("actionName", actionName);
                        action.put("actionResult", actionResult);
                        action.put("shouldCauseWin", shouldCauseWin);
                        action.put("shouldKillAdventurer", shouldKillAdventurer);
                        action.put("shouldTeleport", shouldTeleport);
                        action.put("shouldDisappear", shouldDisappear);
                        action.put("transformTo", transformTo);
                        action.put("score", score);
                        action.put("woundValue", woundValue);
                        itemActions.put(action);
                        s=in.next();
                    }
                    i.put("actions", itemActions);
                    items.put(i);
                    read=in.next();
                }
                else if (type.equals("Rooms:"))
                {
                    JSONObject room=new JSONObject();
                    JSONArray roomContents = new JSONArray();
                    room.put("roomName", read);
                    read=in.next();
                    if (read.contains("Contents:"))
                    {
                        String temp=read;
                        temp=temp.substring(temp.indexOf(":")+2);
                        while (temp.contains(","))
                        {
                            String itemName = temp.substring(0, temp.indexOf(","));
                            roomContents.put(itemName);
                            temp=temp.substring(temp.indexOf(",")+1);
                        }
                        roomContents.put(temp);
                        read=in.next();
                    }
                    room.put("contents", roomContents);
                    in.useDelimiter("---");
                    read+=in.next();
                    room.put("description", read);
                    in.useDelimiter("\n");
                    read=in.next();
                    read=in.next();
                    rooms.put(room);
                }
                else if (type.equals("Exits:"))
                {
                    JSONObject exit = new JSONObject();
                    exit.put("existsInRoom", read);
                    String dir=in.next();
                    String dest=in.next();
                    exit.put("direction", dir);
                    exit.put("destination", dest);
                    exits.put(exit);
                    in.next();
                    read=in.next();
                }
                else
                {
                    System.err.println("Invalid File");
                }
            }
            else
            {
                read=in.next();
            }
        }
        obj.put("rooms", rooms);
        obj.put("items", items);
        obj.put("exits", exits);
        in.close();
        try
        {
            FileWriter fw=new FileWriter(fileName.substring(0, fileName.indexOf(".zork"))+"_converted.zork");
            BufferedWriter out=new BufferedWriter(new PrintWriter(fw));
            obj.write(out);
            out.close();
        }
        catch (IOException e)
        {
            System.err.println(e);
        }
    }
}