# jmud
A text adventure engine in Java. Part of a class project for University Of Mary Washington CPSC240.

Randomization is the name of the game. This will try to adhere to the **invisible dice roll** mud format.

An object oriented Zork engine clone. Not yet as robust but it's getting there.
